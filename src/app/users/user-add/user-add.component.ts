import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import {FormGroup} from '@angular/forms';

@Component({
  templateUrl: './user-add.html'
})
export class UserAddComponent {

  createUserForm: FormGroup;
  user: User = new User();
  error: string;

  constructor(private router: Router, private userService: UserService) {

  }

  createUser(): void {
    this.userService.createUser(this.user)
      .subscribe( success => {
        alert('User created successfully.');
      }, error => {console.log(error);
                   this.error = error.error; });
  }

}
